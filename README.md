## Micrus Assetic ##

This is a module for [Micrus framework](https://micrus.avris.it) that allows you
to integrate it with [Assetic Asset Management](https://github.com/kriswallsmith/assetic).

Instead of including a lot of different CSS stylesheets and JS script in your page,
just let Assetic compile them all into one, thus really speeding up your page.

To install this module, open the file `app/Config/modules.yml` and add:

     - Avris\Micrus\Assetic\AsseticModule

Then run:

    composer require avris/micrus-assetic

Also, make sure that `web/assetic` directory is writable by the server.

Micrus Assetic looks for assets in `app/Asset` directory, applies filters to them
(like Coffeescript, Sass or Uglify), and puts the result in `/web/assetic` with a unique filename to boost caching.

All you have to do is configure a list of filters and assets in `app/Config/config.yml`,
and then include your generated assets in your template.

Sample config looks like this:

    assetic:
      filters:
        sass: [Sass\SassFilter /usr/local/bin/sass]     # new Sass\SassFilter('/usr/local/bin/sass')
        coffee: CoffeeScriptFilter                      # new CoffeeScriptFilter()
        uglifyCss: UglifyCssFilter
        uglifyJs: UglifyJsFilter
      assets:
        css:
          inputs:
            - lib/bootstrap.min.css
            - lib/font-awesome.min.css
            - lib/select2/select2.css
            - lib/select2/select2-bootstrap.css
            - lib/double-slider.css
            - [ sass/*, sass ]                          # take all the files in "sass" dir and apply "sass" filter to them
          filters: ?uglifyCss                           # filters with "?" are only applied in prod env
        js:
          inputs:
            - lib/jquery-2.1.3.min.js
            - lib/bootstrap.min.js
            - lib/select2/select2.min.js
            - lib/bootstrap.btn-number.js
            - lib/double-slider.js
            - [ coffee/* , coffee ]
          filters: ?uglifyJs
      statics:
        - [ lib/select2/select2x2.png, select2x2.png ]  # copy or link app/Asset/lib/select2/select2x2.png to web/assetic/select2x2.png
        - font/fontawesome-webfont.eot                  # copy or link app/Asset/font/fontawesome-webfont.eot to web/assetic/font/fontawesome-webfont.eot
        - font/fontawesome-webfont.svg
        - font/fontawesome-webfont.ttf
        - font/fontawesome-webfont.woff
        - font/fontawesome-webfont.woff2

It uses the same data format as Assetic's Asset Factory, just in the `.yml` file,
so look into [Assetic documentation](https://github.com/kriswallsmith/assetic) for reference.

Using the assets in Twig templates is as easy as that:

    <link href="{{ asset(assets.css) }}" type="text/css" rel="stylesheet" />

    <script src="{{ asset(assets.js) }}"></script>

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
