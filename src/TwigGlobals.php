<?php
namespace Avris\Micrus\Assetic;

class TwigGlobals extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /** @var AsseticManager */
    protected $manager;

    public function __construct(AsseticManager $manager)
    {
        $this->manager = $manager;
    }

    public function getGlobals()
    {
        return [
            'assets' => $this->manager->buildAll(),
        ];
    }

    public function getName()
    {
        return 'assetic_twig_globals';
    }
}
