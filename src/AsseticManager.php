<?php
namespace Avris\Micrus\Assetic;

use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Tool\Cache\CacheWarmupEvent;

class AsseticManager
{
    /** @var Generator */
    protected $generator;

    /** @var AsseticExtension[] */
    protected $extensions;

    /** @var string */
    protected $webDir;

    /** @var Cacher */
    protected $cacher;

    /** @var bool */
    protected $debug;

    public function __construct(Generator $generator, $extensions, $webDir, Cacher $cacher, $env)
    {
        $this->generator = $generator;
        $this->extensions = $extensions;
        $this->webDir = $webDir;
        $this->cacher = $cacher;
        $this->debug = $env === 'dev';
    }

    public function buildAll()
    {
        return $this->cacher->cache('assetic', function () {
            $assets = [];

            foreach ($this->extensions as $extension) {
                $assets = array_merge(
                    $assets,
                    $this->generator->generate(
                        $extension->getConfig(),
                        $extension->getSourceDir(),
                        $this->webDir,
                        $this->debug
                    )
                );
            }

            return $assets;
        });
    }

    public function getAsset($name, $absolute = false)
    {
        $assets = $this->buildAll();

        if (!isset($assets[$name])) {
            return false;
        }

        return ($absolute ? $this->webDir . '/' : '') . $assets[$name];
    }

    public function getWebDir()
    {
        return $this->webDir;
    }

    public function onCacheClear(CacheClearEvent $event)
    {
        $event->removeDirectory($this->webDir . '/assetic');
    }

    public function onCacheWarmup(CacheWarmupEvent $event)
    {
        $this->buildAll();
    }
}
