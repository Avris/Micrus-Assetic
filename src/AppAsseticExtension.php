<?php
namespace Avris\Micrus\Assetic;

class AppAsseticExtension implements AsseticExtension
{
    protected $rootDir;

    protected $config;

    public function __construct($rootDir, $config)
    {
        $this->rootDir = $rootDir;
        $this->config = $config;
    }

    public function getSourceDir()
    {
        return $this->rootDir;
    }

    public function getConfig()
    {
        return $this->config;
    }
}
