<?php
namespace Avris\Micrus\Assetic;

interface AsseticExtension
{
    public function getSourceDir();

    public function getConfig();
}