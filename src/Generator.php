<?php
namespace Avris\Micrus\Assetic;

use Assetic\AssetManager;
use Assetic\AssetWriter;
use Assetic\Factory\AssetFactory;
use Assetic\Factory\Worker\CacheBustingWorker;
use Assetic\FilterManager;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Bag\Bag;

class Generator
{
    public function generate($config, $source, $destination, $debug)
    {
        $config = new Bag($config);

        $factory = new AssetFactory($source, $debug);
        $factory->setAssetManager($am = new AssetManager());
        $factory->setFilterManager($fm = new FilterManager());

        $this->loadFilters($fm, $config->get('filters'));
        $assets = $this->loadAssets($factory, $config->get('assets'), $destination);
        $this->loadStatics($config->get('statics'), $source, $destination);

        return $assets;
    }

    protected function loadFilters(FilterManager $fm, $filters)
    {
        if (empty($filters)) {
            return;
        }

        foreach ($filters as $name => $data) {
            if (!is_array($data)) {
                $data = [$data];
            }
            $class = '\Assetic\Filter\\' . $data[0];
            if (!class_exists($class)) {
                throw new InvalidArgumentException(sprintf('Class %s does not exist', $class));
            }
            unset($data[0]);
            $reflection = new \ReflectionClass($class);
            $filter = $reflection->newInstanceArgs($data);
            $fm->set($name, $filter);
        }
    }

    protected function loadAssets(AssetFactory $factory, $assets, $destination)
    {
        if (empty($assets)) {
            return [];
        }

        $worker = new CacheBustingWorker();
        $writer = new AssetWriter($destination);
        $am = $factory->getAssetManager();

        $generatedAssets = [];

        foreach ($assets as $name => $data) {
            $asset = $factory->createAsset(
                isset($data['inputs']) ? $data['inputs'] : [],
                isset($data['filters']) ? $data['filters'] : [],
                isset($data['options']) ? $data['options'] : []
            );
            $am->set($name, $asset);
            $worker->process($asset, $factory);
            if (!file_exists($destination . '/' . $asset->getTargetPath())) {
                $writer->writeAsset($asset);
            }
            $generatedAssets[$name] = $asset->getTargetPath();
        }

        return $generatedAssets;
    }

    protected function loadStatics($statics, $sourceDir, $destinationDir)
    {
        if (empty($statics)) {
            return;
        }

        foreach ($statics as $data) {
            $source = $sourceDir . '/' . (is_array($data) ? $data[0] : $data);
            $target = $destinationDir . '/assetic/' . (is_array($data) ? $data[1] : $data);

            if (!file_exists($source)) {
                throw new InvalidArgumentException(sprintf('File %s does not exist', $source));
            }

            if ($this->tryLinking($source, $target)) {
                continue;
            }

            if (is_dir($source)) {
                $it = new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS);
                $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::SELF_FIRST);

                foreach ($files as $item) {
                    if ($item->isDir()) {
                        $this->makeDir($target . '/' . $it->getSubPathName());
                    } else {
                        $this->copyFile($item, $target . '/' . $it->getSubPathName());
                    }
                }
            } else {
                $this->copyFile($source, $target);
            }
        }
    }

    protected function tryLinking($source, $target)
    {
        if (file_exists($target)) {
            if (!is_link($target)) {
                return false;
            }

            if (readlink($target) === $source) {
                return true;
            } else {
                unlink($target);
            }
        }

        return symlink($source, $target);
    }

    protected function makeDir($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }

    protected function copyFile($source, $target)
    {
        if (!file_exists($target) || filemtime($source) > filemtime($target)) {
            $this->makeDir(dirname($target));
            copy($source, $target);
        }
    }
}
