<?php
namespace Avris\Micrus\Assetic;

use Avris\Micrus\Bootstrap\Module;

class AsseticModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'asseticManager' => [
                    'class' => AsseticManager::class,
                    'params' => ['@asseticGenerator', '#asseticExtension', '{@rootDir}/web', '@cacher', '@env'],
                    'events' => ['cacheClear', 'cacheWarmup'],
                ],
                'appAsseticExtension' => [
                    'class' => AppAsseticExtension::class,
                    'params' => ['{@rootDir}/app/Asset', '@config.assetic'],
                    'tags' => ['asseticExtension'],
                ],
                'asseticGenerator' => [
                    'class' => Generator::class,
                ],
                'asseticTwigGlobals' => [
                    'class' => TwigGlobals::class,
                    'params' => ['@asseticManager'],
                    'tags' => ['twigExtension'],
                ],
            ],
            'assetic' => [
                'filters' => [],
                'assets' => [],
                'statics' => [],
            ],
        ];
    }
}
